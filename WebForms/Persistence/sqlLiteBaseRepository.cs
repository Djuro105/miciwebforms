﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Web;

namespace WebForms.Persistence
{
    public abstract class SqlLiteBaseRepository
    {

        private string _dbPath = HttpContext.Current.Server.MapPath("\\db.sqlite");

        private void InitializeDatabase()
        {
            SQLiteConnection.CreateFile(_dbPath);


            using (var connection = new SQLiteConnection("Data Source =" + _dbPath))
            {
                connection.Open();

                var cmd = new SQLiteCommand("CREATE TABLE todo (" +
                        "id integer primary key autoincrement, " +
                        "message text, " +
                        "aktivnost integer" +
                    ");",
                    connection);


                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO todo(message, aktivnost) values " +
                    "('Ovo je inicijalni todo', 1)";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "INSERT INTO todo(message, aktivnost) values " +
                    "('2 Ovo je inicijalni todo', 0)";
                cmd.ExecuteNonQuery();
                /*cmd.CommandText = "INSERT INTO users(username, firstName, lastName, email, password, authority) values " +
                    "('Pero12', 'Janko', 'Grgic', 'grga@vub.hr', 'test', 1)";
                cmd.ExecuteNonQuery();
                */
            }
        }

        public SqlLiteBaseRepository()
        {
            if (!File.Exists(_dbPath))
            {
                InitializeDatabase();
            }
        }

        public SQLiteConnection GetDbConnection()
        {
            return new SQLiteConnection("Data Source =" + _dbPath);
        }

    }
}


