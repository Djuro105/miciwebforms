﻿using System.Collections.Generic;
using WebForms.Model;

namespace WebForms.Persistence.Abstractions
{
    interface Itodoreporsitory
    {
        ToDo GetUserById(int Id);

        List<ToDo> GetAll();

        List<ToDo> GetAllActive();

        List<ToDo> GetAllInactive();

        void DeleteAllInactive();

        void DeleteById(int id);

        void ToggleActive(int id);

        void insertTodo(ToDo todo);

        void EditById(ToDo todo);

    }
}