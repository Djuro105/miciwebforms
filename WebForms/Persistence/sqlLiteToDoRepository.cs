﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebForms.Model;
using WebForms.Persistence.Abstractions;

namespace WebForms.Persistence
{
    public class SqlLiteToDoRepository : SqlLiteBaseRepository, Itodoreporsitory
    {
        public void DeleteAllInactive()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("DELETE FROM todo WHERE aktivnost = 0");
                    
            }
        }


        public void DeleteById(int id)
        {
            using (var connection = GetDbConnection())
            {
                try
                {
                    connection.Open();
                    var users = connection.Execute("DELETE FROM todo WHERE id = @id",
                        new { id = id });

                }
                catch (Exception ex)
                {
                    
                    throw;
                }
            }
        }

        public void EditById(ToDo todo)
        {
            throw new NotImplementedException();
        }


        public List<ToDo> GetAll()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var todos = connection.Query<ToDo>("SELECT id, message, aktivnost FROM todo");

                return todos.ToList(); 
            }
        }


        public List<ToDo> GetAllActive()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var todos = connection.Query<ToDo>("SELECT id, message, aktivnost FROM todo WHERE aktivnost = 1");

                return todos.ToList();
            }
        }

        public List<ToDo> GetAllInactive()
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var todos = connection.Query<ToDo>("SELECT id, message, aktivnost FROM todo WHERE aktivnost = 0");

                return todos.ToList();
            }
        }

        public ToDo GetUserById(int Id)
        {
            throw new NotImplementedException();
        }

        public void insertTodo(ToDo todo)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("INSERT INTO todo(message, aktivnost) VALUES (" +
                    "@message,  @aktivnost) ",
                    new
                    {
                        message = todo.Message,
                        aktivnost = 1
                    });
            }
        }

        public void ToggleActive(int id)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("UPDATE todo SET aktivnost = 0 where id = @id ",
                    new
                    {
                        id = id
                    }); ;
            }

        }
    }
}