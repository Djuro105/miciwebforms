﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebForms.Model;
using WebForms.Persistence;
using WebForms.Persistence.Abstractions;

namespace WebForms
{
    public partial class _Default : Page
    {
        private Itodoreporsitory _todoRepository;
        protected void Page_Load(object sender, EventArgs e)
        {
            _todoRepository = new SqlLiteToDoRepository();

            if (!IsPostBack) // ucitava podatke samo prvi put
            {
                //ResetForm();  
                GetTodosData(); 

                
            }


        }

        private void GetTodosData()
        {
            var todos = _todoRepository.GetAll();

            gvTodos.DataSource = todos;
            gvTodos.DataBind();
        }

        protected void Active_Click(object sender, EventArgs e)
        {
            var todos = _todoRepository.GetAllActive();

            gvTodos.DataSource = todos;
            gvTodos.DataBind();
        }

        protected void InActive_Click(object sender, EventArgs e)
        {
            var todos = _todoRepository.GetAllInactive();

            gvTodos.DataSource = todos;
            gvTodos.DataBind();
        }

        protected void All_Click(object sender, EventArgs e)
        {
            GetTodosData();
        }

        protected void DeleteInactive_Click(object sender, EventArgs e) {
            _todoRepository.DeleteAllInactive();
            GetTodosData();
        }

        protected void Create_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNewMessage.Text))
            {
                var todo = new ToDo
                {
                    Message = txtNewMessage.Text,
                    Aktivnost = 0
                };

                _todoRepository.insertTodo(todo);
                GetTodosData();
            }
        }

        protected void DeleteByID_Click(object sender, EventArgs e)
        {
            var todoId = (sender as Button).CommandArgument;
            _todoRepository.DeleteById(Convert.ToInt32(todoId));
            GetTodosData();
        }

        protected void SetRijeseno_Click(object sender, EventArgs e)
        {
            var todoId = (sender as Button).CommandArgument;
            _todoRepository.ToggleActive(Convert.ToInt32(todoId));
            GetTodosData();
        }

    }
}