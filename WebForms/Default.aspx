﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br /> <br />
        <asp:GridView ID="gvTodos" CssClass="table" runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="Id todo-a" />
            <asp:BoundField DataField="message" HeaderText="Poruka todo-a" />
            <asp:BoundField DataField="aktivnost" HeaderText="Aktivnost todo-a" />
            <asp:TemplateField>
                <ItemTemplate>
                   <asp:Button ID="Button6" Text="Delete" runat="server" OnClick="DeleteByID_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                    <asp:Button ID="Button7" Text="Oznaci kao rjeseno" runat="server" OnClick="SetRijeseno_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    Add new todo <asp:TextBox ID="txtNewMessage" runat="server" CssClass="form-control"></asp:TextBox>
        <asp:Button ID="Button5" runat="server" Text="Create New Todo" OnClick="Create_Click" /> <br />
    <asp:Button ID="Button3" runat="server" Text="Pokazi sve" OnClick="All_Click" />
    <asp:Button ID="Button1" runat="server" Text="Pokazi active" OnClick="Active_Click" />
    <asp:Button ID="Button2" runat="server" Text="Pokazi inactive" OnClick="InActive_Click" />
    <asp:Button ID="Button4" runat="server" Text="Obrisi inactive" OnClick="DeleteInactive_Click" />

</asp:Content>
