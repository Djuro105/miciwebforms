﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForms.Model
{
    public class ToDo
    {
            public int Id { get; set; }
            public string Message { get; set; }
            public int Aktivnost { get; set; }        
    }
}